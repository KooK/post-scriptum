# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'https://kook.frama.io/post-scriptum'
RELATIVE_URLS = False

FEED_MAX_ITEMS = 5
FEED_ALL_RSS = 'feeds/all.rss.xml'

# Ces fichiers sont générés mais pas utilisés ?
CATEGORY_FEED_RSS = 'feeds/{slug}.rss.xml'
AUTHOR_FEED_RSS = 'feeds/{slug}.rss.xml'
TAG_FEED_RSS =  'feeds/{slug}.rss.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

DISQUS_SITENAME = "https-kook-frama-io-post-scriptum.disqus.com"
#GOOGLE_ANALYTICS = ""
