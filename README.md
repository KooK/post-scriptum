# post-scriptum

blog perso.
bioinfo, info, sciences et autres.

## Avant propos

Pelican utilise Python, donc avant de commencer, créer un virtualenv pour ce projet.

```shell
virtualenv venv_blog
# du blabla

source venv_blog/bin/activate
(venv_blog) $
```

## INSTALL

```shell
(venv_blog) $ pip install -r requirements.txt 
# blabla de téléchargement

# démarrer le serveur
#	les pages du dossier 'content' seront automatiquement générées
(venv_blog) $ make devserver
```

## Déploiement

Actuellement les pages sont déployées sur `http://kook.frama.io/post-scriptum`
