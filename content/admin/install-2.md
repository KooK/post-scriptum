Title: Déploiement du blog
Date: 2023-03-25
Modified: 2023-03-25
Category: informatique
Tags: administration,blog
Slug: déploiement-du-blog
Authors: KooK
Summary: Deuxième article, utiliser gitlab-pages pour déployer le blog.
Status: published

Pour ce 2e article, le but est d'expliquer comment rendre le blog visible / disponible sur le web.  
Pour ça, on va utiliser les `GitlabPages` qui permettent justement de publier des sites web statiques directement depuis un dépôt Gitlab.  
Je décris ce que j'ai fait pour le blog basé sur Pelican, mais il existe [beaucoup](https://about.gitlab.com/blog/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/) de générateurs de site.


# Les bases

## Gitlab

Gitlab est une forge logiciel libre basée sur [`git`](https://fr.wikipedia.org/wiki/Git) qui propose un service de [déploiement continue](https://fr.wikipedia.org/wiki/D%C3%A9ploiement_continu), et le service de mise à disposition de sites web statiques `GitlabPages`.  

La première étape sera donc de mettre les fichiers de configuration de `Pelican` et le premier article sur une [plateforme gitlab](https://framagit.org/KooK/post-scriptum).

## GitlabPages

L'[intro](https://docs.gitlab.com/ee/user/project/pages/introduction.html) sur les pages Gitlab nous apprend qu'il y a 4 pré-requis:

- avoir un nom de domaine (=connaitre l'URL d'accès au site web)
- un fichier gitlab-ci contenant un job `pages`
- le contenu du site dans un dossier nommé `public`
- un runner gitlab actif (en général, il y en a au moins un fourni sur chaque plateforme gitlab)

## Gitlab-CI

Gitlab propose un service d'intégration continue (CI), ce sont des pipelines qui peuvent s'exécuter lorsque l'on pousse du code sur le dépôt.  
Ces pipelines sont décrits dans un fichier nommé `.gitlab-ci.yml` (format yaml).  

Si vous ne connaissez pas du tout Gitlab-CI, aller lire l'intro sur le sujet sur le blog de [Stéphane Robert](https://blog.stephane-robert.info/post/introduction-gitlab-ci/)


# Création du projet

Pour créer un projet de blog de votre côté vous pouvez :

- créer un nouveau projet vide
- créer votre projet à partir d'une divergence (`fork`) d'un autre projet
- créer votre projet à partir d'un modèle (`template`)

Je passe sur les commandes `git` nécessaires pour envoyer les fichiers sur le dépôt git ; ce n'est pas l'objet de l'article.  
Disons que d'une façon ou d'une autre, on a notre projet dans une instance de Gitlab.  


# Décryptons le fichier gitlab-ci.yml

Comme mentionné plus haut, il définit le pipeline d'intégration continue du projet.  
D'abord il faut définir une image docker. C'est à peu de chose une machine sur laquelle travailler.

```yaml
image: python:alpine
```
Ici, on va bosser dans un environnement où python sera disponible dans sa dernière version sur un OS alpine (distribution Gnu/Linux de très petite taille).  
cf [le hub docker](https://hub.docker.com/_/python?tab=tags) pour voir le détail de ce que contient l'image.

Pour notre dépôt, nous allons définir deux tâches :

 - une de *test* qui lancera la commande `make publish`
 - une de *déploiement* qui lancera la même commande mais placera le résultat dans un dossier `public` qui sera défini comme un `artefact` à conserver.

## la tâche de test

Cette tâche **n'est pas nécessaire** pour le déploiement du blog, mais ça me permet d'introduire l'écriture de tâche.  
Son nom sera "test" et sera affecté à l'étape (`stage`) "test" ! :D  
Un pipeline est divisé en étape / `stage` qui ne peuvent que s'exécuter l'un après l'autre.  
Et un `stage` contient des tâches qui elles peuvent s'exécuter en parallèle.  

La liste des commandes à exécuter contient l'installation de `make`, des paquets pip et pour finir la commande `make publish`.

J'ajoute une règle qui dit que la tâche ne sera faite que si on n'est pas sur la branche `master`.  
Ce qui donne :
```yaml
test:
  stage: test
  script:
    - apk add --update-cache --no-cache make
    - pip install -r requirements.txt
    - make publish
  rules:
    # si on ne pousse pas sur master (master = $CI_DEFAULT_BRANCH)
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
```

`apk` est le système de gestion de paquets du système Alpine.  
L'option `--update-cache` signifie de mettre à jour le cache au moment de la commande et `--no-cache` permet de ne pas conserver les infos sur les paquets localement.

## la tâche de déploiement

Cette tâche **est obligatoire** pour générer un site statique.  
Une contrainte, pour que Gitlab détecte cette tâche comme une tâche pour GitlabPages, elle doit s'appeler "pages".  
Je vais la mettre dans l'étape "deploy" et changer la condition pour qu'elle ne soit exécutée que lorsqu'on est sur la branche master.

La liste des commandes est identique à une exception, on ajoute un paramètre au `make publish` pour préciser le dossier de sortie. En effet, pour être exploitable, les fichiers doivent se trouver dans un dossier nommé "public".  
Pour que le dossier "public" existe après l'exécution du pipeline, il faut le déclarer dans une section "artifact".

Et voilà :
```yaml
pages:
  stage: deploy
  script:
    - apk add --update-cache --no-cache make
    - pip install -r requirements.txt
    - make publish OUTPUTDIR=public
  artifacts:
    paths:
    - public/
  rules:
    # si on pousse sur master (master = $CI_DEFAULT_BRANCH)
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

## Bonus: petite optimisation

Comme vous le voyez, chacune des tâches fait l'installation des mêmes dépendances. Normal, ce sont quasi les mêmes tâches.  

D'un point de vue sémantique, ce n'est pas une action de la tâche, mais une préparation de l'environnement d'exécution.  
De fait je préfère mettre ces 2 lignes dans une section `before_script`. Cette section sera toujours exécutée avant la section `script`.

Et puisqu'on a 2 fois la même section `before_script`, il suffit de dire à gitlab que cette section est à faire quelque soit la tâche, avec la déclaration d'une section `default`.
```yaml
default:
  before_script:
    - apk add --update-cache --no-cache make
    - pip install -r requirements.txt
```


# Le pré-requis non traité

Il y a 4 pré-requis, on n'a pas abordé le runner parce qu'il est fourni par la plateforme, mais où trouvé l'adresse d'accès de notre blog ?

`Paramètres -> Pages -> Access pages`

```
Your pages are served under:
http://kook.frama.io/post-scriptum
```

# Pour en savoir plus

allez à la [source](https://docs.gitlab.com/ee/user/project/pages/#gitlab-pages)  
la syntaxe du [YAML ](https://yaml.org/)  
Notez bien que le fichier `gitlab-ci.yml` que j'ai décrit est celui fournit par le modèle de projet Gitlab Pelican, à l'optimisation prêt peut-être.  
