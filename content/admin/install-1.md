Title: Installation du blog
Date: 2023-01-02
Modified: 2023-01-02
Category: informatique
Tags: administration,blog
Slug: installation-du-blog
Authors: KooK
Summary: Premier article, description de l'installation du blog
Status: published

Pour ce blog, j'ai choisi un système de génération de site statique pour leur simplicité de mise en œuvre
et j'ai choisi [Pelican](https://getpelican.com) parce que je l'avais déjà utilisé. Je ne veux pas perdre trop de temps sur l'outil, je veux me focaliser sur le contenu.

On trouve beaucoup de tutoriels comme par exemple sur [zestedesavoir.com](https://zestedesavoir.com/tutoriels/2497/creer-un-site-web-statique-avec-pelican/)
(attention, le site n'utilise pas le Makefile).


# Installation de Pelican

`Pelican` est écrit en `Python`, donc on commence par créer un *environnement virtuel*.
(je pars d'un système Gnu/Linux basé sur Debian, avec un terminal)

```shell
# installation de virtual-env pour isoler
apt install python3-virtualenv
virtualenv -p python3 venv_blog

# on activera l'environnement chaque fois qu'on voudra bosser sur le blog
source venv_blog/bin/activate

# on pourra désactiver avec `deactivate`

# pour finir on lance un serveur
make devserver
```

Voilà pour commencer on a ce qu'il faut.

`devserver` permet de lancer un serveur pour profiter de votre site en local (avant publication).
Vous pouvez aussi visualiser les pages html directement en ouvrant les fichiers html générés (dans le dossier output),
mais vous n'aurez pas l'utilisation du *thème*.

Vient ensuite l'écriture d'un premier article.

## Du contenu pour débuter

`Pelican` gère les formats [reStructuredText](https://docutils.sourceforge.io/rst.html) (`rst`) pour le contenu nativement et [Markdown](https://daringfireball.net/projects/markdown/) (`md`) (avec l'installation du package Markdown).
Les 2 formats permettent de définir une mise en forme sommaire tout en restant dans un bloc-note, et sans nuire à la lecture dudit contenu sans autre mise en forme.

Dans un premier temps, j'ai choisi le `rst` parce que j'utilise déjà `md` pour la doc au boulot, et que je voulais tester cet autre format.
Le coût d'apprentissage est faible quand on connait déjà un système de *formattage en texte brut*.

Après quelques essais, je suis revenu au format `md` pour avoir des retours à la ligne. C'est balo, mais il semble que `rst` ne permette pas de le faire.
De plus le soulignage des titres est un peu lourd en cas de changement.  
Contrainte: il faut installer Markdown.

```shell
pip install Markdown
```

Et donc enfin le premier contenu :

```md
Title: "article" minimal
Date: 2022-10-16 12:34
```
À noter que ceci (un titre et une date) suffit pour pouvoir générer une page, ce sera un article vide [ici]({filename}/samples/article_mini.md).  
Mais pour montrer plus, on va en écrire un peu plus.

```markdown
Title: Installation du blog
Date: 2022-05-15 19:00
Modified: 2022-10-16 12:45
Category: informatique
Tags: informatique, administration,
Slug: article-0
Authors: KooK
Summary: Premier article, description de l'installation du blog

Pour ce blog, j'ai choisi un système de génération de site statique pour leur simplicité de mise en œuvre
et j'ai choisi [Pelican](https://getpelican.com) parce que je l'avais déjà utilisé. Je ne veux pas perdre trop de temps sur l'outil, je veux me focaliser sur le contenu.

On trouve beaucoup de tutoriels comme par exemple sur [zestedesavoir.com](https://zestedesavoir.com/tutoriels/2497/creer-un-site-web-statique-avec-pelican/)
(attention, le site n'utilise pas le Makefile).
```

Voilà, on a quelques *méta-données* utiles, du contenu et des liens, ça suffira pour la démo.
En enregistrant le fichier dans `content/samples/article0.md` vous remarquerez (si votre serveur tourne) que la [page html]({filename}/samples/article0.md) a bien été générée.

```shell
-> Modified: content. re-generating...
Done: Processed 1 article, 0 drafts, 0 hidden articles, 0 pages, 0 hidden pages and 0 draft pages in 0.27 seconds.
```

Et pour finir on va installer un thème.

## Mise en forme du contenu

Le fichier html généré est utilisable seul, mais son rendu est un peu fade. Ouvrez le fichier dans un navigateur pour vous en rendre compte (file://...).
Si vous ouvrez le fichier via le rendu du serveur (http://127.0.0.1:8000/) vous verrez que la mise en forme est plus pimpante.
(je ne comprends pas vraiment pourquoi le thème n'est pas accessible directement, après tout le css ce sont aussi des fichiers statiques)

Pour lister les thèmes déjà disponibles

```shell
pelican-themes --list
simple
notmyidea
```
Ce sont là les 2 thèmes livrés avec pelican. `notmyidea` est le thème par défaut.  
Pour en changer, il va falloir ajouter la variable `THEME` dans le fichier `pelicanconf.py`.

```shell
# ajouter la ligne avec un éditeur de texte est bien sur suffisant
# mais, là je frime
cat >> pelicanconf.py
THEME = "simple"

# et je termine avec ctrl+d pour indiquer une fin de fichier
```

Pour en obtenir de nouveaux, proposés par la communauté des utilisateurs, il faut aller les chercher sur github [ici](https://github.com/getpelican/pelican-themes)

Mais pour ça je dois installer git (rappelez-vous, je suis sur une machine neuve).

```shell
apt install git

# clonage du dépôt
git clone --recursive https://github.com/getpelican/pelican-themes ~/pelican-themes
```

Commencez par regarder les fichiers `readme` de chaque thème, ils comportement des captures d'écran.  
Ensuite pour tester un thème, remplacez la valeur de la variable `THEME` par le chemin absolu vers ce thème.

```shell
# je frime un max
perl -pi -e 's#THEME = "simple"#THEME = "'$HOME'/pelican-themes/pelican-sober"#' pelicanconf.py
```
Un petit rafraichissement dans le navigateur pour voir si le résultat convient et si c'est le cas, on peut officialiser cette union en installant le thème dans pelican.
L'avantage est ensuite de ne plus se trainer le chemin absolu du thème, mais simplement son nom.

```shell
pelican-themes --install "$HOME/pelican-themes/pelican-sober"
pelican-themes --list
pelican-sober
simple
notmyidea
```
Le thème est maintenant disponible dans notre environnement et on peut mettre à jour une fois de plus la variable `THEME`.
```shell
perl -pi -e 's#THEME = "'$HOME'/pelican-themes/pelican-sober"#THEME = "pelican-sober"#' pelicanconf.py
```

Voilà pour la démo, en ce qui me concerne, je reste sur le thème par défaut pour le moment, n'en ayant pas trouvé de plus plaisant et ne voulant pas passer plus de temps sur cette installation !


En vous souhaitant la bonne journée :)
