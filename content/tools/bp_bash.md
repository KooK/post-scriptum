Title: Bash, trucs et bonnes pratiques
Date: 2023-08-14
Modified: 2023-11-11
Category: informatique
Tags: tools,bash
Slug: bash-trucs-et-bonnes-pratiques
Authors: KooK
Summary: Des trucs et de bonnes pratiques pour Bash
Status: published

Ça fait plusieurs années que j'écris des scripts bash et régulièrement je découvre de nouvelles astuces, de nouveaux mots clefs.  
Je vais les noter ici à mesure.


# Variables

## Les tableaux

```sh
declare -a l_=( a b c d e )
l_+=( 33 "en plusieurs mots" )

for a in "${l_[@]}"
do
	printf "%s\n" "$a"
done
```

### recueillir la sortie d'un grep dans une liste

```sh
# readarray (or mapfile) read element from a file and store them as variable array
#   -t : Remove a trailing delim
readarray -t l_names <<< \
	$( grep -v -e ^# any_file.lst )
```

# Redirection

## code d'exemple script.sh

```sh
#! /bin/bash

printf "sortie d'erreur\n" >&2
printf "sortie standard\n" >&1

```

## rediriger stdout vers un fichier **sans stderr**

```sh
$ ./script.sh > out
sortie d'erreur

$ ./script.sh 1> out
sortie d'erreur
```

## rediriger stderr vers un fichier **sans stdout**

```sh
$ ./script.sh 2> out
sortie standard
```

## rediriger stdout **et** stderr vers des fichiers

```sh
$ ./script.sh > out 2> err
```

## rediriger stdout **et** stderr vers le **même** fichier

```sh
$ ./script.sh > out 2>&1

# à noter: '> out' est avant la redirection de stderr vers stdout
#	l'inverse ne fonctionne pas.
```
