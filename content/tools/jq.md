Title: Découverte de jq
Date: 2023-07-30
Modified: 2023-07-30
Category: informatique
Tags: tools,jq,json
Slug: découverte-de-jq
Authors: KooK
Summary: Premiers pas avec jq, un outils de manipulation de chaînes json
Status: published

J'explorais l'[API REST Ensembl](http://rest.ensembl.org/) et à défaut de trouver ce que je cherchais, il y a là une bonne occasion de parler de **`jq`** [https://stedolan.github.io/jq/](https://stedolan.github.io/jq/).

`jq` est probablement l'acronyme de Json Query.
C'est un programme pour analyser une chaine JSON et permettre d'en extraire des infos ou de restructurer son contenu.

## étape 0 : obtenir un fichier json

```
wget -q --header='Content-type:application/json' http://rest.ensembl.org/info/species  -O ensembl_species.json
```

Cette commande liste toutes les espèces disponibles avec certaines informations associées et ce, bien entendu sous la forme d'une chaine json (une seule ligne), le résultat est enregistré dans le fichier `ensembl_species.json`.
Sa forme ressemble à : `{"species":[{ ... }]}`.  
On a donc un dictionnaire avec au moins une clef `"species"` dont la valeur est une liste de dictionnaires/objets.

## Premiers pas avec jq

`jq [options] <jq filter> [file...]`
jq applique des filtres sur la chaine json, et c'est dans ce filtre que réside toute la difficulté d'utilisation du programme.

### le plus simple des filtres '.'

Le plus simple des filtres permet d'avoir une sortie "pour humain" (pretty-print).

```
jq '.' ensembl_species.json

{
  "species": [
    {
      "assembly": "PanLeo1.0",
      "taxon_id": "9689",
      "release": 108,
      "division": "EnsemblVertebrates",
      "groups": [
        "core"
      ],
      "name": "panthera_leo",
      "aliases": [],
      "display_name": "Lion",
      "accession": "GCA_008795835.1",
      "strain_collection": null,
      "common_name": "lion",
      "strain": null
    },
    {
      "common_name": "denticle herring",
      "strain": null,
      "assembly": "fDenClu1.1",
    ...
```
et déjà ça nous donne le format des objets/dictionnaires de la liste de "species".

### Lister les clefs du dictionnaire

Le filtre peut contenir certains mots-fonctions, comme `keys` ou `length`.

```
jq 'keys' ensembl_species.json
[
  "species"
]
```

### et savoir combien d'objets contient la liste d'espèces

Pour lister la valeur associée à "species", on peut specifier cette clef dans le filtre

```
jq '.species' ensembl_species.json | head -n 20
[
  {
    "assembly": "PanLeo1.0",
    "taxon_id": "9689",
    "release": 108,
    "division": "EnsemblVertebrates",
    "groups": [
      "core"
    ],
    "name": "panthera_leo",
    "aliases": [],
    "display_name": "Lion",
    "accession": "GCA_008795835.1",
    "strain_collection": null,
    "common_name": "lion",
    "strain": null
  },
  {
    "common_name": "denticle herring",
    "strain": null,
```

autre propriété des filtres, on peut les enchainer avec des tubes (pipe |) comme dans le shell
```
jq '.species | length' ensembl_species.json
316
```

On conclue donc que nous avons 316 espèces dans notre fichier.

## Plus compliqué

Je veux reformatter l'entrée pour avoir la taille associée à chaque clef.  
Bon ici l'intérêt est assez limité puisque la commande précédente nous donne déjà le résultat. Mais si on a plus d'une clef dans le premier dictionnaire, voici comment obtenir la taille de toutes les listes/valeurs :

```
jq 'map_values(length)' ensembl_species.json
{
  "species": 316
}
```

La commande est simple, mais si on débute avec jq, on peut passer très longtemps à chercher.  
Je sais utiliser jq pour accéder aux éléments d'un json, mais pour réaliser ce reformattage, j'ai cherché quelques heures !

```
printf '{ "un":[1,2,3], "deux": [1,2] }' \
    | jq 'map_values(length)'
{
  "un": 3,
  "deux": 2
}
```

## analyse des objets

### une option
Comme on l'a vu, par défaut jq produit une sortie "jolie", mais il y a une option qui permet d'annuler ce comportement.

```
jq --compact-output '.' ensembl_species.json
{"species":[{"assembly":"PanLeo1.0","taxon_id":"96 ...
```

Ici clairement, ça ne sert à rien puisque l'entrée est égale à la sortie, mais elle peut nous permettre de répondre à la prochaine question.

### Est-ce que tous les objets de "species" sont identiques ?
Il faut avouer que si la réponse est non, alors on peut douter de la source !  
Disons que c'est pour l'exercice.  
L'idée ici serait de vérifier que tous les objets "species" ont les mêmes attributs, et donc d'appliquer la fonction "keys" sur chaque objet. Pour ça, on peut faire appel à l'operateur d'itération `.[]`.

```
jq '.species[] | keys' ensembl_species.json | head -n 20
[
  "accession",
  "aliases",
  "assembly",
  "common_name",
  "display_name",
  "division",
  "groups",
  "name",
  "release",
  "strain",
  "strain_collection",
  "taxon_id"
]
[
  "accession",
  "aliases",
  "assembly",
  "common_name",
  "display_name",
```

Avant de revenir à la question, on notera que la sortie n'est pas une liste de listes, mais un ensemble de listes. Petite différence qu'il est important de noter.
Si ces listes d'attributs étaient en ligne, on pourrait utiliser la commande `uniq` pour répondre à notre question.  
Et c'est là que l'option de sortie compacte intervient :

```
jq --compact-output '.species[] | keys' ensembl_species.json | uniq -c
    316 ["accession","aliases","assembly","common_name","display_name","division","groups","name","release","strain","strain_collection","taxon_id"]
```
On a donc bien 316 dictionnaires/objets structurés de la même façon.

## reformatage de la sortie

Pour le moment, j'ai juste besoin de pouvoir parcourir le nom des espèces, je voudrais donc une sortie de type dictionnaire avec comme clef le nom scientifique (normalement unique) et le nom courant.

Voilà ma réflexion pour construire le filtre.  
Pour chacun des objets de la liste 'species', je veux le contenu des clefs 'name' et 'display_name'.

```
jq '.species[] | [.display_name, .name] ' ensembl_species.json | head                                                   jq
[
  "Mangrove rivulus",
  "kryptolebias_marmoratus"
]
[
  "Round goby",
  "neogobius_melanostomus"
]
```

Ok, en jouant, je peux obtenir aussi un dictionnaire en changeant le 2e élément du filtre pour '{k: .display_name, v: .name }'.

Mais je ne veux pas ces clefs 'k' et 'v'. Je l'avoue, je suis allé demander à l'internet et voilà la première solution:
```
jq '.species[] | {(.display_name): .name } ' ensembl_species.json | head                                                jq
{
  "Mangrove rivulus": "kryptolebias_marmoratus"
}
{
  "Round goby": "neogobius_melanostomus"
}
{
  "Dog - Great Dane": "canis_lupus_familiarisgreatdane"
}
```

C'est presque ce que je veux. Là j'ai un ensemble (pas une liste au sens jq) de dictionnaires d'une seule clef. Je voudrais concaténer tout ça.  
En fouillant un peu la doc, je comprends que ma solution doit inclure les fonctions 'add' et/ou 'map'. Et là, ben j'ai testé un peu toutes les combinaisons, mais ce qui m'a permis de trouvé, c'est la doc de la fonction map : `map(x) is equivalent to [.[] | x]`  
je peux donc utiliser correctement le map en enlevant le `[]` de mon 1er élément, et là j'obtiens une liste de dictionnaire d'un seule clef. Je colle la fonction `add` après la constitution de cette liste et bingo !
```
jq '.species | map({(.display_name): .name }) | add' ensembl_species.json | head                                        jq
{
  "Mangrove rivulus": "kryptolebias_marmoratus",
  "Round goby": "neogobius_melanostomus",
  "Dog - Great Dane": "canis_lupus_familiarisgreatdane",
  "Zebra mbuna": "maylandia_zebra",
  "Mouse WSB/EiJ": "mus_musculus_wsbeij",
```

## Réflexions

On l'a aperçu, jq est un outil très puissant.  
Le hic, c'est que pour maitriser cette puissance il faut beaucoup d'entrainement. Or en général, on veut utiliser ce genre d'outils pour aller vite, et perso j'ai vu des collègues préférer écrire un petit script ad-hoc pour faire leur reformattage. Comment leur en vouloir, ça fonctionne, et c'est souvent plus rapide. Mais c'est une solution rapide à court terme, parce que des données à extraire et à reformatter, on en a beaucoup.  
Beaucoup mais en général pas assez pour justifier de passer des heures à se former sur cet outil en particulier !

Aussi selon moi la bonne façon de faire à titre individuel, c'est d'essayer de l'utiliser, toujours. Quand on trouve une solution, on l'a stocke dans un fichier accessible, avec des commentaires.
À titre collectif, il serait bien qu'au sein d'une équipe, il y ait une personne experte sur l'outil, et sinon, que les petits succès individuels soient systématiquement partagés et compris. Le copier-coller sauvage ne vous fera pas progesser.
