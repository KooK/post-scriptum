Title: Installation du blog
Date: 2022-05-15 19:00
Modified: 2022-10-16 12:45
Category: informatique
Tags: informatique, administration,
Slug: article-0
Authors: KooK
Summary: Premier article, description de l'installation du blog
Status: hidden

Pour ce blog, j'ai choisi un système de génération de site statique pour leur simplicité de mise en œuvre
et j'ai choisi [Pelican](https://getpelican.com) parce que je l'avais déjà utilisé. Je ne veux pas perdre trop de temps sur l'outil, je veux me focaliser sur le contenu.

On trouve beaucoup de tutoriels comme par exemple sur [zestedesavoir.com](https://zestedesavoir.com/tutoriels/2497/creer-un-site-web-statique-avec-pelican/)
(attention, le site n'utilise pas le Makefile).
